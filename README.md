# Questions

### Why it is not advised to use innerHTML in JavaScript?

innerHTML content is refreshed every time and thus is slower. 
There is no scope for validation in innerHTML and, therefore, it is easier to insert rouge code in the document and, thus, make the web page unstable.

### What is the difference between '==' and '===' operators?

== checks only for equality in value
=== returns true when the two operands are having the same value without any type conversion

### Define event bubbling

Event propagation in the HTML DOM API.
With bubbling, the event is first captured and handled by the innermost element and then propagated to outer elements. Event delegates from bottom to top, thus why it's called bubbling.

### ECMAScript 6 - name some of the new important features

- Immutable variables
- Lexical this (intuitive handling of current object context)
- Custom/String Interpolation (${expression})
- Unicode String and RegExp Literal
- Class Definition and Inheritance (class Class extends BaseClass {…})
- Base Class Access
- Static Members (Class.implementedFunction())
- Promise Combination (Promise.all([…]).then((data) => {…}, (error) => {}))
- Proxying. Hooking into runtime-level object meta-operations

### MVC frameworks - name some of them, name some you used and define pros and cons of these frameworks

- AngularJS
- Dart
- AngularDart
- TypeScript + AngularJS
- Ember.js
- Mithril
- KnockoutJS (+ RequireJS)
- Backbone.js
- Vue.js
- Meteor

#### AngularJS:

##### PROS:
- Excels at quick prototyping and simple dynamic SPA
- Easy testability
- Really obvious two way data-binding
- Dirty checking
- Angular Expressions
- Directives (small app inside app philosophy takes it to comfort level of using reusable components)
- Huge amount out-of-the-box components which are really helpful 
- Lightweight
- Polymer support, which potentially in near future can be quite a time-saver for building web apps based on Material Design
- Google as developer, which takes care of popularity and third-parties
- Third party support is excellent nowadays

##### CONS:
- Native project structure is really bad, scalability of app is horrendous if not taken care on step of architecture prototype
- Big projects often is difficult to maintain and update in terms of features if it's architecture was designed wrong from the start
- Native router is limited (ng-route will be included by default in AngularJS 2)
- Version bump up to AngularJS 2 from the old versions is almost impossible, app needs to be developed almost from scratch 
- In early days there were no code helpers and good documentation
- Scopes are sometimes tricky to debug

#### Ember.js:

##### PROS:
- Pods. Introduced the ECMAScript6 architecture for components, which doesn't require to store heaps of files in couple of folders
- Models (ember-data). If REST architecture is shaped for the standard format - it's really easy to get data, including associated models inside the response, possibility to use hasMany|belongsTo
- Template-engine HTMLBars (formerly Handlebars)
- Native broccoli integration with terminal (ember-cli)
- Custom views. Obvious implementation gui
- Custom helpers
- Third-party support. Lots of libraries, almost always you can find required library for the task
- Ember-inspector browser addon. Really nice implementation of inspector, allows to dig everything you need including 
the guts of base implementations or first-party components
- Computed properties and aliases
- Default router is a joy to use with pods
- Pretty good performance so far
- Fixtures for placeholder data
- Excellent i-18n third-party module

##### CONS:
- Versions. Each update is a pain, requires to remove/add quirks specific for a version or a component which extends
the time of development, sometimes, significantly.
- Compatibility of modules sometimes could be totally broken on the update. Even from first-party components.
- Models. Ruby-like JSON model serialization, almost no option if REST isn't shaped specifically for the web app, which 
requires to write down the proxy-server if app is intended to be multi-platform. Sometimes even custom serializator 
for the component isn't enough and you need to modify BaseSerializator.
- High entry point
- No out of the box implementation of everyday use libraries (validation, datepickers, etc.)
- Custom views. Sometimes it can be painful to implement DOM-dependant modules (ex. jQuery-datepicker, GoogleCAPTCHA v.2)
- Third-party support. Lots of libraries, which are outdated
- HTMLBars pollution of the DOM
- Almost bloating size of mediocre (scale of difficulty and components quantity) app, sometimes it can bite in slow
 first time loading, even after minification+obfusification of the code
- Today it's almost impossible to use ECMAScript5 flavor of Ember, only adequate to use ember-cli  
 
### Name possible attacks on JavaScript applications. How would you prevent these attacks? What can be done to improve performance of web page?

##### Types of attacks
- Client XSS
- Server XSS
- Reflective XSS
- Persistent XSS
- Stored XSS

##### Means to prevent attacks:
- Don't use innerHTML, instead use innerText or textContent (popular frameworks take care of this automatically)
- Backend security
- Obfuscate and minify the code
- Untrusted data should only be treated as displayable text and validated
- String escaping
- Template system with context-aware auto-escaping
- Black-box-testing
- Writing unit tests to verify correct escaping or sanitization 
- Automated tools and/or security scanners

##### Means to improve performance:
- Minimizing HTTP Requests (achievable with help of localStorage and Cookies, building app into 2 files, etc.)
- Minifying HTML, CSS and JavaScript
- CSS files only in the <head>
- Sometimes third-party libraries could be omitted to <footer> for app to load faster without visual eye-candy
- Optimizing Images (incl. usage of sprites, scaling)
- Caching
- Google Page Speed usage to detect slopes in performance in certain parts
- Manipulate fragments before adding them to DOM
- Sometimes excessive use of (for ... in ...) can be performance-degrading
- Using the task runners such as Grunt/Broccoli/Gulp with appropriate tests/optimizations to be done automatically 

### What are differences between local and session storage? When would you use them? Which Javascript libraries do you like and use the most and why?

##### localStorage:
- survives page reloads
- persists over different tabs or windows
 
##### sessionStorage: 
- available for the duration of the browser session
- deleted after the window is closed
- persists over different tabs or windows
- survives page reloads

##### USAGE:
Perfect for persisting non-sensitive data which needed within client scripts between pages (for example: preferences, small chunks of history data in tables, customizable elements parameters, etc.)

### What kinds of tests can be done in Javascript application? Which tools or libraries would you use for testing or writing tests?

##### Front-end apps
- User interface testing
- User acceptance testing
- Performance testing
- Manual support testing
- Functionality testing
- Browser compatibility testing and configuration testing
- Scalability testing
- Load testing
- Security testing
- Functional testing

##### Back-end apps
- Data volume testing
- Stress testing
- Scalability testing
- Load testing

##### Testing tools:
- Jasmine
- Mocha
- Karma
- JSLint
- Protractor
- QUnit


# phone-list-ng

## Running

Just host the dist dir on MAMP/LAMP/WAMP stack, build is minimized and uglified

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.
