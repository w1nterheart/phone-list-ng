'use strict';

/**
 * Created by exiri on 10/3/15.
 */

angular.module('phoneListGruntApp.services', ['ngResource'])
  .service("phonesService", function($log, localStorageService, $window, $rootScope) {
    var Phone = function Phone(data) {
      //semi-id
      if (typeof data.created === "undefined" || data.created === 0) {
        data.created = new Date().getTime();

        var recordDate = new Date(data.time);
        var date = new Date();
        date.setHours(recordDate.getHours());
        date.setMinutes(recordDate.getMinutes());
        data.time = date;
        data.done = date.getTime() < data.created;
      }

      angular.copy(data, this);
    };

    Phone.prototype.edit = function () {
      this.$editting = !this.$editting;
    };

    Phone.prototype.save = function () {
      return update(this).then(function () {
        this.$editting = false;
      }).bind(this);
    };

    /** @private **/
    var _localStorageKey = "phones";
    var broadcastKey = 'LocalStorageModule.notification.setItem';

    var getAll = function () {
      var records = localStorageService.get(_localStorageKey);
      var result = [];
      if (records !== null) {
        for (var i = 0; i < records.length; i++) {
          if (records[i] !== null) {
            result.push(new Phone(records[i]));
          }
        }
      }

      return result;
    };

    var get = function (created) {
      var records = localStorageService.get(_localStorageKey);
      if (!parseInt(created) || records === null) {
        return false;
      }

      for (var i = 0; i < records.length; i++) {
        if (records[i].created === created) {
          return records[i];
        }
      }

      return null;
    };

    var create = function(data) {
      var records = localStorageService.get(_localStorageKey);
      if (records === null) {
        records = [];
      }

      records.push(new Phone(data));
      if (!localStorageService.set(_localStorageKey, records)) {
        return false;
      }

      //Update current instance
      _broadcastChange();
      return true;
    };

    var update = function (data) {
      var records = localStorageService.get(_localStorageKey);
      if (!data || !data.created || records === null) {
        return false;
      }

      for (var i = 0; i < records.length; i++) {
        if (records[i].created === data.created) {
          data.updated = new Date().getTime();
          records[i] = new Phone(data);
          break;
        }
      }

      if (!localStorageService.set(_localStorageKey, records)) {
        return false;
      }

      _broadcastChange();
      return true;
    };

    var remove = function (created) {
      var records = localStorageService.get(_localStorageKey);
      if (records === null) {
        //Probably outdated data
        return true;
      }

      for (var i = 0; i < records.length; i++) {
        if (records[i] !== null && records[i].created === created) {
          delete records[i];
          break;
        }
      }

      if (!localStorageService.set(_localStorageKey, records)) {
        return false;
      }

      _broadcastChange();
      return true;
    };

    var removeAll = function () {
      localStorageService.set(_localStorageKey, []);
      _broadcastChange();
    };

    var _broadcastChange = function () {
      $rootScope.$broadcast(broadcastKey, {updated: true});
    };

    //To keep data updated across the tabs. This is a simple-scoped LS change, so no logic is implemented
    // to differentiate what's changed
    angular.element($window).on('storage', function(event) {
      _broadcastChange();
      $rootScope.$apply();
    });

    return {
      update    : update,
      create    : create,
      getAll    : getAll,
      get       : get,
      remove    : remove,
      removeAll : removeAll,
      broadcastKey: broadcastKey
    };
  });
