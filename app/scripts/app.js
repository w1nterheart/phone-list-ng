'use strict';

/**
 * @ngdoc overview
 * @name phoneListGruntApp
 * @description
 * # phoneListGruntApp
 *
 * Main module of the application.
 */
angular
  .module('phoneListGruntApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'LocalStorageModule',
    'phoneListGruntApp.services'
  ])
  .config(function ($routeProvider, localStorageServiceProvider) {
    localStorageServiceProvider
      .setPrefix('phoneListGruntApp')
      .setNotify(true, true);

    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/history', {
        templateUrl: 'views/history.html',
        controller: 'HistoryCtrl',
        controllerAs: 'history'
      })
      .when('/add', {
        templateUrl: 'views/add.html',
        controller: 'AddCtrl',
        controllerAs: 'add'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
