'use strict';

/**
 * @ngdoc function
 * @name phoneListGruntApp.controller:MainCtrl
 * @description
 * # AddCtrl
 * Controller of the phoneListGruntApp
 * adds numbers
 */
angular.module('phoneListGruntApp')
  .controller('AddCtrl', function ($scope, $log, $rootScope, $filter, $interval, phonesService) {
    $scope.phoneRegex = /^\+\(\d{3}\)(\s\d{3}){3}|\+\(\d{3}\)\-\d{9}|\+\d{12}|0{2}\d{12}$/;

    var _resetModel = function () {
      $scope.model = {
        name: null,
        number: null,
        time: 0,
        created: 0,
        done: false
      };
    };

    var _validatePhone = function (phone) {
      return phone.replace(/^\+/, "00").replace(/[\s\-\(\)\W+]/gm, "");
    };

    _resetModel();
    $scope.save = function() {
      $scope.error = false;
      if (!$scope.model.name || !$scope.model.time || !$scope.model.number) {
        $scope.error = true;
        return;
      }

      $scope.model.number = _validatePhone($scope.model.number);
      if ($scope.model.number && phonesService.create($scope.model)) {
        _resetModel();
      }
    };
  });
