'use strict';

/**
 * @ngdoc function
 * @name phoneListGruntApp.controller:MainCtrl
 * @description
 * # NextCtrl
 * Controller of the phoneListGruntApp
 * displays nearest numbers for the future
 */
angular.module('phoneListGruntApp')
  .controller('NextCtrl', function ($scope, $log, $rootScope, $filter, $interval, phonesService) {
    $scope.model = null;

    var getItems = function () {
      $scope.items = $filter('orderBy')(phonesService.getAll(), "time");
    };

    var displayNextNumber = function () {
      getItems();
      var now = new Date().getTime();
      for (var i = 0; i < $scope.items.length; i++) {
        if ($scope.items[i].time !== null && new Date($scope.items[i].time) > now) {
          $scope.model = $scope.items[i];
          break;
        }
      }
    };

    displayNextNumber();

    //Each 5 minutes refreshes next numbers
    setInterval(function () {
      displayNextNumber();
    }, 300000);

    //Look for the changes in LS
    $scope.$on(phonesService.broadcastKey, function(event, parameters) {
      if (typeof parameters.updated != "undefined" && parameters.updated) {
        displayNextNumber();
      }
    });
  });
