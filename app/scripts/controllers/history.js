'use strict';

/**
 * @ngdoc function
 * @name phoneListGruntApp.controller:MainCtrl
 * @description
 * # HistoryCtrl
 * Controller of the phoneListGruntApp
 * displays table of contents
 */
angular.module('phoneListGruntApp')
  .controller('HistoryCtrl', function ($scope, $log, $rootScope, $filter, $interval, phonesService, localStorageService) {
    $scope.filters = {
      all: true,
      next: false,
      finished: false
    };

    $scope.sortType = "time";
    $scope.sortReverse = false;

    var _records;
    $scope.getItems = function () {
      _records = phonesService.getAll();
      $scope.items = _records;
    };
    $scope.getItems();

    //this implementation backed by faster performance and direct control over the process
    //primitive, but it works
    $scope.filterFinished = function () {
      var now = new Date().getTime();
      $scope.items = [];
      for (var i = 0; i < _records.length; i++) {
        if (new Date(_records[i].time).getTime() < now) {
          $scope.items.push(_records[i]);
        }
      }
    };

    $scope.filterNext = function () {
      var now = new Date().getTime();
      $scope.items = [];
      for (var i = 0; i < _records.length; i++) {
        if (new Date(_records[i].time).getTime() > now) {
          $scope.items.push(_records[i]);
        }
      }
    };

    $scope.removeAll = function () {
      phonesService.removeAll();
    };

    $scope.remove = function (item) {
      phonesService.remove(item.created);
    };

    $scope.update = function (item) {
      item.done = !item.done;
      phonesService.update(item);
    };

    //Look for the changes in LS
    $scope.$on(phonesService.broadcastKey, function(event, parameters) {
      if (typeof parameters.updated != "undefined" && parameters.updated) {
        $scope.getItems();
      }
    });
  });
