'use strict';

/**
 * @ngdoc function
 * @name phoneListGruntApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the phoneListGruntApp
 * acts as observer
 */
angular.module('phoneListGruntApp').controller('MainCtrl', function () {});
